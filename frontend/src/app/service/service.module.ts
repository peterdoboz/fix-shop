import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceComponent } from './service.component';
import {ServiceRoutingModule} from '@app/service/service-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ServiceRoutingModule
  ],
  declarations: [ServiceComponent]
})
export class ServiceModule { }
