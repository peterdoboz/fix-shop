import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route, extract } from '@app/core';
import {ServiceComponent} from '@app/service/service.component';

const routes: Routes = [
  Route.withShell([
    { path: 'service', component: ServiceComponent, data: { title: extract('Service') } }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ServiceRoutingModule { }
